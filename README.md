# Paging Mission Control
The information below provides information about the execution of the submission, which was built with Java 8.
Clone this repository in order to run the program.

## Input File
Drop the input file (.txt) in the ```\input``` directory. The program is dependent on the file being named ```input.txt```

## Compilation
In the command line, navigate to the ```\src``` directory located in the project directory. To compile the project:
```$ javac -cp "../lib/gson-2.10.1.jar" Main.java```.
### A Quick Note:
Because Gson is the only outside library utilised in my solution, I opted not to create extra build files nor use dependencies.

## Execution
Still within the ```\src``` directory: ```$ java -cp ".:../lib/gson-2.10.1.jar" Main```
### Windows
Please note that the Windows execution is slightly different: ```$ java -cp ".;../lib/gson-2.10.1.jar" Main```

## Output
The program will log the output to the console in JSON format.
