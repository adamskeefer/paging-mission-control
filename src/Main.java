/**
 * The following program can be used to analyze telemtry data from
 * satellites in order to generate alert messages based on temperature and battery
 * voltage reading. The program runs on the console and usage instructions can be found
 * in the Readme.md file.
 */


import java.util.*;
import java.io.*;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

public class Main {

    
    public static void main(String[] args)
    {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        /**
         * Key: satelliteID.
         * The use of a hashmap allows for files with more than two satellites to be read without any modification to the code.
         */
        HashMap<Integer, Satellite> satelliteMap = new HashMap<>();
        JsonArray alerts = new JsonArray();
        try (BufferedReader br = new BufferedReader(new FileReader("../input/input.txt")))
        {
            String line;
            String[] lineData;
            int id;
            double highLimit;
            double lowLimit;
            double reading;
            String component;
            String timestamp;
            Satellite satellite;
            Alert alert = new Alert();

            while((line = br.readLine()) != null)
            {
                lineData = line.split("\\|");
                id = Integer.parseInt(lineData[1]);
                highLimit = Double.parseDouble(lineData[2]);
                lowLimit = Double.parseDouble(lineData[5]);
                reading = Double.parseDouble(lineData[6]);
                component = lineData[7];
                timestamp = lineData[0];

                //checks if reading is safe; if so, we move onto next line
                if((reading <= highLimit && component.equals("TSTAT")) && (reading >= lowLimit && component.equals("BATT")))
                    continue;
                
                
                if(!satelliteMap.containsKey(id))
                    satelliteMap.put(id, new Satellite(id, highLimit, lowLimit));
                
                satellite = satelliteMap.get(id);

                //determing the type of alert, and adding to appropriate queue
                if(reading > highLimit && component.equals("TSTAT"))
                {
                    alert = new Alert(id, "RED HIGH", component, timestamp);
                    satellite.addHighQ(alert, alerts);
                }

                if(reading < lowLimit && component.equals("BATT"))
                {
                    alert = new Alert(id, "RED LOW", component, timestamp);
                    satellite.addLowQ(alert, alerts);
                }
                  
            }
            System.out.println(gson.toJson(alerts));
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}

class Alert {
    int satelliteId;
    String severity;
    String component;
    String timestamp;

    Alert()
    {
        satelliteId = 0;
    }
    Alert(int id, String sev, String comp, String time)
    {
        this.satelliteId = id;
        this.severity = sev;
        this.component = comp;
        this.timestamp = time;
    }

    int getId()
    {
        return this.satelliteId;
    }

    String getSeverity()
    {
        return this.severity;
    }

    String getTimestamp()
    {
        return this.timestamp;
    }

    String getComponent()
    {
        return this.component;
    }

    /**
     * Converts alert into JsonObject and adds it to the JsonArray of alerts.
     * Note: An Ordered map was used because gson JsonObjects are not saved in
     * any specific order.
     */
    void printAlert(JsonArray array)
    {
        Gson gson = new Gson();
        String datetime = getTimestamp();
        String date = datetime.split(" ")[0];
        String time = datetime.split(" ")[1];

        date = date.substring(0,4) + "-" + date.substring(4,6) + "-" + date.substring(6);
        LinkedHashMap<String, Object> orderedMap = new LinkedHashMap<>();
        orderedMap.put("satelliteId", getId());
        orderedMap.put("severity", getSeverity());
        orderedMap.put("component", getComponent());
        orderedMap.put("timestamp", date + "T" + time + "Z");
        JsonObject jsonObject = gson.toJsonTree(orderedMap).getAsJsonObject();
        array.add(jsonObject);

        return;
    }
}

class Satellite {
    int id;
    double redHigh;
    double redLow;
    Queue<Alert> lowAlertQueue = new LinkedList<>(); //a.k.a the battery alerts
    Queue<Alert> highAlertQueue = new LinkedList<>(); //a.k.a the termostat alerts

    Satellite(int id, double high, double low)
    {
        this.id = id;
        this.redHigh = high;
        this.redLow = low;
    }

    double getHigh()
    {
        return this.redHigh;
    }

    double getLow()
    {
        return this.redLow;
    }

    Queue<Alert> getLowQ()
    {
        return this.lowAlertQueue;
    }

    Queue<Alert> getHighQ()
    {
        return this.highAlertQueue;
    }

    /*
     * Alerts are added to a satellite specific queue containing the same type of alerts.
     * If the queue size is 3, the time of the first and most recent addition to the queue are
     * checked. These methods do not return anything, but rather call upon printAlert method, printing
     * the popped alert.
     */
    void addLowQ(Alert a, JsonArray array)
    {
        Queue<Alert> queue = getLowQ();
        queue.add(a);
        if (queue.size() == 3)
        {
            Alert firstAlert = queue.remove();
            if (compare(a, firstAlert))
                firstAlert.printAlert(array);
        }
        return;
    }

    void addHighQ(Alert a, JsonArray array)
    {
        Queue<Alert> queue = getHighQ();
        queue.add(a);
        if(queue.size() == 3)
        {
            Alert firstAlert = queue.remove();
            if (compare(a, firstAlert))
                firstAlert.printAlert(array);
        }      
        return;

    }

    //comparing the times of the two alerts
    boolean compare(Alert a, Alert b)
    {
        String timestampA = a.getTimestamp();
        String dayA = timestampA.split(" ")[0];
        String timeA = timestampA.split(" ")[1];
        String timestampB = b.getTimestamp();
        String dayB = timestampB.split(" ")[0];
        String timeB = timestampB.split(" ")[1];

        if(!(dayA.equals(dayB)))
            return false;
        
        if(Math.abs(ChronoUnit.MINUTES.between(LocalTime.parse(timeA), LocalTime.parse(timeB))) <= 5)
            return true;
            
        return false;
    }
}